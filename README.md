## VELO MOBILE
Sistema mobile desenvolvido para avaliação de conhecimentos acadêmicos.

# Name
VELO

# Description
Software para dispositivos mobile. O app mostra a velocidade de deslocamento  do dispositivo além de possuir bússola para orientação.

# Installation
Para instalar o app no celular (Android) basta colocar o arquivo "app-debug-7.apk" dentro do celular pelo cabo usb e depois clicar no mesmo para iniciar a instalação.

# Authors
Carlos Henrique Venâncio Filho; [Júlio Cesar Nomelini](https://gitlab.com/jcesarnomelini)

# License
Software disponível para fins acadêmicos. Proibído a venda.

# Project status
Projeto está encerrado. (Finalizado)
